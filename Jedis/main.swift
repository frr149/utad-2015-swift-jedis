//
//  main.swift
//  Jedis
//
//  Created by Fernando Rodríguez Romero on 27/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

import Foundation

// Agregados: Enum, Structs, Classes, Tuplas

// Extensiones
extension String{
    
    func reverse()->String{
        var r = ""
        r.extend(Swift.reverse(self))
        return r
    }
    mutating func mreverse() {
        self = self.reverse()
    }
}

println("tengo un cimbel matutino".reverse())
var s = "onitutam lebmic nu ognet"
s.mreverse()
println(s)


// Enums
enum LightSaberColor{
    case Red, Blue, Green, Purple
}

// Structs
struct LightSaber {
    
    // Class or static property
    static let quote = "An elegant weapon for a more civilized age"
    
    // Instance properties
    var color : LightSaberColor = .Blue {
        // This is an observer
        willSet{
            println("About to change color to \(color)")
        }
    }
    
    var isDoubleBladed = false
    
}

LightSaber.quote
var jediSaber = LightSaber()
jediSaber.color = .Green
var sithSaber = LightSaber(color: .Red, isDoubleBladed: true)


// Classes
class Sentient {
    
    var name = ""
    
    
    init(){}            // default init
    init(name:String){  // designated init
        self.name = name
    }
}

var jabba = Sentient(name:"Jabba the Hutt")


class Jedi: Sentient {
    
    // MARK : - Propiedades
    var midichlorians = 42
    var lightSaber = LightSaber(color: .Blue, isDoubleBladed: false)
    var padawanOf: Jedi?    // Añadimos Optionals. con esto ya es nil
    
    // MARK: - Init
    
    // Crea un Jedi completo
    convenience init(name: String, saber: LightSaber, midichlorians: Int){
        self.init(name: name)
        
        self.lightSaber = saber
        self.midichlorians = midichlorians
        
    }
    
    // Crea un maestro con 10K midiclorianos
    // demás props por defecto
    convenience init(masterName: String){
        self.init(name: masterName)
        midichlorians = 10000
    }
    
    
    // MARK: - Métodos normales
    // Métodos normales
    func empuñadura() ->String{
        return "||||||(•)"
    }
    
    func hoja() ->String{
        return "█Ξ█████████████████████"
    }
    
    func pomo()->String{
        return "█"
    }
    
    func unsheathe() -> String{
        
        // ¡Funciones dentro de funciones!
        func saber() ->String{
            return pomo() + empuñadura() + hoja()
        }
        
        func doubleSaber() ->String{
            return hoja().reverse() + empuñadura() + hoja()
        }
        
        if !lightSaber.isDoubleBladed{
            return saber()
        }else{
            return doubleSaber()
        }
    }

    
}


var qui = Jedi(name: "Qui Gon Jinn",
    saber: LightSaber(color: .Green, isDoubleBladed: false), midichlorians: 4000);

var yoda = Jedi(masterName: "Mich Yoda")
qui.padawanOf = yoda

yoda.unsheathe()
qui.padawanOf?.name


class Sith: Jedi {
    
    // Crea un señor oscuro del Sith.
    // de conveniencia
    convenience init(darkLordName: String){
        self.init(masterName: darkLordName)
        lightSaber = LightSaber(color: .Red, isDoubleBladed: false)
        midichlorians = 10_000
    }
    
    // Sobrecargamos un método de Jedi
    override func empuñadura() -> String {
        return "◚◚◚◚◚ịị▔ịị"
    }
    
    override func hoja() -> String {
        return "≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣≣"
    }
    
    override func pomo() -> String {
        return "ị▔ịị"
    }

    
    
}

var vader = Sith(darkLordName: "Darth Vader")
vader.unsheathe()

var maul = Sith(darkLordName: "Darth Maul");
maul.lightSaber.isDoubleBladed = true
println("Sith with funky sword: \(maul)\n\(maul.unsheathe())")

println(yoda.unsheathe())







